"""Module desined for loading and ploting data from /data/ directory."""

import pickle
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import k, physical_constants, angstrom
from potencials import slfunctions

ev = physical_constants["electron volt"][0]

scales = {
    "nondimensional": {
        "e": "nondimensional units",
        "x": "nondimensional units"
    },
    "angstrom": {
        "e": "eV",
        "x": r"$\AA$"
    },
    "angstrom_mass": {
        "e": "eV",
        "x": r"$\AA\cdot\mathrm{amu}^{\frac{1}{2}}$"
    },
    "si": {
        "e": "J",
        "x": r"m"
    }
}


def get_result(pname, delta_x=0, delta_e=0, scale=None, reverse=False,
               params=None):
    """Reads results from data file with given by `pname`"""

    if params is None:
        params = {}

    ##############################
    #    Scale transformations
    ##############################
    orig_scale = slfunctions[pname]["scale"]

    if scale is None:
        scale = orig_scale
    else:
        assert scale in scales, "Undefined scale {}!".format(scale)

    # Transformation to si units
    x_trans = slfunctions[pname]["x_to_si"]
    e_trans = slfunctions[pname]["e_to_si"]
    required_params = (
        slfunctions[pname]["e_params"] + slfunctions[pname]["x_params"])

    if scale != orig_scale:
        for i in required_params:
            if i not in params:
                raise Exception("{} mus be passed!".format(i))

    if scale == orig_scale:
        x_transform = lambda x: x
        e_transform = lambda x: x
    elif scale == "si":
        x_transform = lambda x: x_trans(x, **params)
        e_transform = lambda x: e_trans(x, **params)
    elif scale == "angstrom":
        x_transform = lambda x: x_trans(x, **params)/angstrom
        e_transform = lambda e: e_trans(e, **params)/ev
    elif scale == "angstrom_mass":
        x_transform = lambda x: (
            x_trans(x, **params)*np.sqrt(params["m"])/angstrom)
        e_transform = lambda e: e_trans(e, **params)/ev
    else:
        raise TypeError("Undefined scale {}!".format(scale))

    ##############################
    #       Reading data
    ##############################

    with open("data/{}.p".format(pname), "rb") as data_file:
        result = pickle.loads(data_file.read())

    potencial = result["potencial"]
    eigenspectrum = result["eigenspectrum"]

    if reverse:
        sign = -1
        reverse = lambda x: list(reversed(x))
    else:
        sign = 1
        reverse = lambda x: x

    potencial["x"] = reverse([sign*x_transform(i) + delta_x
                              for i in potencial["x"]])
    potencial["y"] = reverse([e_transform(i) + delta_e
                              for i in potencial["y"]])

    for eresult in eigenspectrum:
        eresult["energy"] = e_transform(eresult["energy"]) + delta_e
        eresult["x"] = reverse([sign*x_transform(i) + delta_x
                                for i in eresult["x"]])
        eresult["y"] = reverse([e_transform(i)
                                for i in eresult["y"]])

    result["scale"] = scale
    result["x_units"] = scales[scale]["x"]
    result["e_units"] = scales[scale]["e"]
    result["params"] = params
    result["name"] = pname
    return result


def plot_potencial(result, eigenstates=False, axes=None, show=True,
                   title=True, marker=None, label=None, **params):
    if axes is None:
        axes = plt

    if marker is None:
        axes.plot(result["potencial"]["x"],
                  result["potencial"]["y"],
                  label=label, **params)
    else:
        axes.plot(result["potencial"]["x"],
                  result["potencial"]["y"],
                  marker, label=label, **params)

    try:
        axes.xlabel("$x$ ({})".format(result["x_units"]))
        axes.ylabel("$E$ ({})".format(result["e_units"]))
    except:
        pass

    axes.grid(True)

    name = "{} potencial".format(result["name"])
    if eigenstates:
        name = "{} eigenspectrum".format(result["name"])
        plot_eigenspectrum(result, axes)

    # if result["scale"] == "si":
    #     axes.title(name + "(m = {:.5f}kg)".format(result["params"]["m"]),
    #                y=1.08)
    # elif result["scale"] in {"angstrom", "angstrom_mass"}:
    #     axes.title(
    #         name + " (m = {} amu)".format(
    #             result["params"]["m"]), y=1.08)

    if show:
        axes.show()


def plot_potencials(result1, result2, eigenstates=False, axes=None, show=True):
    if axes is None:
        axes = plt

    plot_potencial(result1, eigenstates=eigenstates, axes=axes, show=False)
    plot_potencial(result2, eigenstates=eigenstates, axes=axes, show=False)

    # axes.title("{}\n{}".format(result1["name"], result2["name"]))

    if show:
        axes.show()


def calculate_distribution(result, temp=270):
    """
    Returns:
    - E (list): energy in nondimetional form
    - E_tranformed (list): energy in SI units
    - P (list): probability of states
    """
    if temp == 0:
        temp = 1

    beta = pow((temp * k), -1)

    if result["scale"] is None:
        raise TypeError(
            "To calculate equilibrium distribution scale mus be given!")

    E_SI = [i["energy"] for i in
            get_result(result["name"], scale="si",
                       params=result["params"])["eigenspectrum"]]

    pp = [np.exp(-beta*e) for e in E_SI]
    Z = sum(pp)

    P_n = lambda n: pp[n]/Z
    P = [P_n(i) for i in np.arange(len(E_SI))]

    E = [i["energy"] for i in result["eigenspectrum"]]
    return E, P


def plot_eigenspectrum(result, axes=None, show_energy=True, each=5):
    if axes is None:
        axes = plt

    # rescaling for representation
    E = []
    for i in result["eigenspectrum"]:
        E.append(i["energy"])

    E = np.array(E)
    diffs = E[1:] - E[:-1]
    my_min = diffs.min()*2
    my_max = max(result["eigenspectrum"][0]["y"])

    factor = my_max/my_min
    # factor = factor*4

    for nr, i in enumerate(result["eigenspectrum"]):
        # if nr == 7:
        #     break
        if not nr % each:
            axes.plot(i["x"],
                      np.array(i["y"])/factor + i["energy"],
                      linewidth=1)
            if show_energy:
                axes.text(i["x"][-1] + 0.2, i["energy"],
                          r'$E={0:.3f}$'.format(i["energy"]))
                axes.text(i["x"][0]-0.5, i["energy"],
                          r'${}$'.format(nr))

    axes.xlim([-6, 11])
    axes.ylim([-3, 2.2])


def plot_distribution(result, temp=270, axes=None):
    if axes is None:
        axes = plt

    E, P = calculate_distribution(result, temp)
    axes.title("Thermal equilibrium distribution at T={}K".format(temp))
    axes.ylabel("Prob")
    axes.xlabel("E ({})".format(result["e_units"]))
    axes.grid(True)
    axes.plot(E, P, "bo")
    axes.show()
