"""Module where all Surm-Liouvile functions are stored.
Also convertion to si units methods are atached to each function.

Also running this module:

$ python3 potencials.py

generates data files for all described potencials"""

from scipy.constants import hbar, physical_constants, m_p, angstrom
from numpy import sqrt

electron_volt = physical_constants["electron volt"][0]
amc = physical_constants["atomic mass constant"][0]

slfunctions = {}

##############################
#    Harmonic oscilator
##############################
# Harmonic oscilator is calculated
# in nondimensional units


# Sturm-Liouvile function
def sturm_liouvile_function_hc(x, energy=0):
    return 2 * energy - pow(x, 2)


# Rescaling parameters to SI units
# omega is given in \hbar\omega = eV
omega_t = lambda omega: (omega*electron_volt)/hbar
# mass is given in amu
m_t = lambda m: m*amc


# transformation to si units
e_to_si = lambda e, **kwargs: e*omega_t(kwargs["omega"])*hbar
x_to_si = lambda x, **kwargs: x*sqrt(
    hbar/(m_t(kwargs["m"])*omega_t(kwargs["omega"])))


slfunctions["harmonic"] = {
    "scale": "nondimensional",
    "function": sturm_liouvile_function_hc,
    "e_to_si": e_to_si,
    "x_to_si": x_to_si,
    "e_params": ["omega"],
    "x_params": ["omega", "m"]
}

##############################
#   Anharmonic potencial 1
##############################


def anharmonic_potencial_am(x, F=239.22534, omega=0.0367, beta=0.03,
                            gamma=0.0035, anharmonic=True):
    if anharmonic:
        return 0.5*F*pow(omega, 2)*pow(x, 2) + beta*pow(x, 3) + gamma*pow(x, 4)
    return 0.5*F*pow(omega, 2)*pow(x, 2)


def sturm_liouvile_function_a(x, energy):
    scale = 2e-20*m_p*electron_volt/pow(hbar, 2)
    return scale*(energy - anharmonic_potencial_am(x))


slfunctions["anharmonic"] = {
    "scale": "angstrom_mass",
    "function": sturm_liouvile_function_a,
    "e_to_si": lambda e, **params: e*electron_volt,
    "x_to_si": lambda x, **params: x*angstrom/sqrt(params["m"]),
    "e_params": [],
    "x_params": ["m"]
}


##############################
#   Anharmonic potencial 1
##############################

def sturm_liouvile_function_a2(x, energy):
    scale = 2e-20*m_p*electron_volt/pow(hbar, 2)
    return scale*(energy - anharmonic_potencial_am(
        x, F=239.22534, omega=0.026, beta=-0.004, gamma=0))

slfunctions["anharmonic2"] = {
    "scale": "angstrom_mass",
    "function": sturm_liouvile_function_a2,
    "e_to_si": lambda e, **params: e*electron_volt,
    "x_to_si": lambda x, **params: x*angstrom/sqrt(params["m"]),
    "e_params": [],
    "x_params": ["m"]
}


##############################
#   Anharmonic potencial 1
##############################

# def sturm_liouvile_function_a3(x, energy):
#     scale = 2e-20*m_p*electron_volt/pow(hbar, 2)
#     return scale*(energy - anharmonic_potencial_am(
#         x, F=240.966, omega=0.02622, beta=-0.004, gamma=0))

# slfunctions["anharmonic3"] = {
#     "scale": "angstrom_mass",
#     "function": sturm_liouvile_function_a3,
#     "e_to_si": lambda e, **params: e*electron_volt,
#     "x_to_si": lambda x, **params: x*angstrom/sqrt(params["m"]),
#     "e_params": [],
#     "x_params": ["m"]
# }


# from shooting_method import calculate_eigenvalue_spectrum
# calculate_eigenvalue_spectrum(slfunctions["anharmonic2"]["function"],
#                               nr_of_states=80,
#                               save_to_file="data/anharmonic2.p",
#                               verbose=2,
#                               scale=slfunctions["anharmonic2"]["scale"])

if __name__ == "__main__":
    from shooting_method import calculate_eigenvalue_spectrum

    for i in slfunctions:
        print("calculating {}".format(i))
        calculate_eigenvalue_spectrum(slfunctions[i]["function"],
                                      nr_of_states=80,
                                      save_to_file="data/{}.p".format(i),
                                      verbose=2, scale=slfunctions[i]["scale"])
