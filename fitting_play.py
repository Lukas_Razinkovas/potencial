from scipy.optimize import leastsq
import numpy
from matplotlib import pyplot as plt

xdata = numpy.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0])
ydata = numpy.array([0.1, 0.9, 2.2, 2.8, 3.9, 5.1])

plt.plot(xdata, ydata)
plt.show()


def func(params, xdata, ydata):
    return (ydata - numpy.dot(xdata, params))

x0 = numpy.array([0.0, 0.0, 0.0])

xdata = numpy.transpose(numpy.array([[1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                                     [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]]))

print(leastsq(func, x0, args=(xdata, ydata)))
