"""Module designed for finding eigenvalues of Shrodinger equations"""
import pickle
from numerov import numerov_method
import numpy as np
import time
from matplotlib import pyplot as plt

DATA_DIR = "data"


def get_next_eigenvalue(sturm_liouvile_function, start_energy,
                        delta=0.01, precision=1, base=10,
                        borders=3, verbose=True):
    """Searches for the next eigenvalue from `start_energy`.

    Arguments:
      start_energy (float): initial energy from which search will begin
      delta (float): minimum difference from zero at right boundary
      base (int): step size is calculated by formula
        $step = base^{-precision}$, where precision changes dynamicaly.
      precision (int): initial precision value
      verbose (bool): if True print some information

    Returns:
       result (dict): dictionary object with stored energy and
         function values::

          {
              "energy": <int>,
              "x": <list>,
              "y": <list>
          }
    """
    if verbose:
        print("#"*70)
        print(' '*30 + "Start Energy: {}".format(str(start_energy)))
        print("#"*70)

    energy = start_energy + delta
    diff = delta * 2
    sign = 0
    direction = 1

    while diff > delta:
        step = direction * pow(base, -precision)
        energy += step
        result = numerov_method(
            lambda x: sturm_liouvile_function(x, energy),
            borders=borders)
        x, y, roots = result["x"], result["y"], result["roots"]
        diff = abs(y[-1])
        new_sign = np.sign(y[-1]) if np.sign(y[-1]) else 1
        if not sign:
            sign = new_sign

        if verbose:
            print("Energy:{},\tdiff:{},\tstep:{},\tsign:{}".format(
                energy, y[-1], step, sign))

        if new_sign != sign:
            if verbose:
                print("Changing direction!")
            direction = -1*direction
            precision += 1
            sign = new_sign

    if verbose:
        print("-"*70)
        print("Found eigenvalue:" + str(energy))
        plt.plot(x, y)
        plt.show()

    return {"energy": energy,
            "x": x,
            "y": y,
            "roots": roots}


def calculate_eigenvalue_spectrum(sturm_liouvile_function, nr_of_states=50,
                                  start_energy=0, save_to_file=None,
                                  verbose=False, base=10, borders=3):
    """Calculates eigenvalue spectrum from  given Sturm Liouvile Functions.
    If save_to_file is given then saves results with nondimensional units
    in specified file.

    Arguments:
      nr_of_states (int): number of wanted states
      start_energy (float): initial energy from which search will begin
      save_to_file (str): path to file, where results will be stored
      verbose (bool): if True prints some information

    Result:
    
    """
    energy = start_energy
    results = []
    potencial = [(0, 0)]

    for i in range(nr_of_states):
        if verbose:
            print("Looking for eigenvalue nr:{}".format(i), end="... ")
            beg = time.time()

        result = get_next_eigenvalue(sturm_liouvile_function, energy,
                                     borders=borders, base=base,
                                     verbose=True if verbose == 2 else False)

        if verbose:
            print("Finished ({}s)".format(time.time() - beg))

        results.append(result)
        energy = result["energy"]
        potencial += list(
            zip(result["roots"], [energy] * len(result["roots"])))

    potencial = sorted(potencial)
    potencial = np.array(potencial)
    x_potencial = potencial[:, 0]
    y_potencial = potencial[:, 1]
    potencial = {
        "x": x_potencial,
        "y": y_potencial
    }

    results = {
        "eigenspectrum": results,
        "potencial": potencial
    }

    if save_to_file is not None:
        with open(save_to_file, "wb") as out_file:
            pickle.dump(results, out_file)

    return results
