import pickle
import numpy as np
# from get_data import get_result, plot_potencial
# from scipy.constants import k
from matplotlib import pyplot as plt

ticks = []

import matplotlib
# matplotlib.rcParams['savefig.dpi'] = 2.5 * matplotlib.rcParams['savefig.dpi']x

plt.figure(figsize=(10, 7), dpi=100)


def plot_experiment(filename, label="none"):
    E = []
    P = []

    with open(filename, "rt") as f:
        for line in f:
            e, p = line.rstrip().split("\t")
            if float(e) > 3:
                continue
            E.append(float(e))
            P.append(float(p))

    E = np.array(E)
    P = np.array(P)
    P = P/max(P)

    indx = np.argmax(P)
    if filename == "experiment2.txt":
        ticks.append(float("{:.2}".format(E[indx])))
        plt.vlines(E[indx], 0, 1, linestyles='dashed')

    plt.plot(E, P, "o", label=label, alpha=0.7)


def plot_results(with_omega=False, plot="FWHM"):
    for name in ["hh", "aa"]:
        with open("data/{}_results{}.p".format(
                name, "" if with_omega else "_no_w"), "rb") as data_file:
            results = pickle.loads(data_file.read())
            for result in results:
                if result["T"] == 100:
                    if name == "hh":
                        delta = 0.7
                    else:
                        delta = 0.07
                    delta = 0
                    name = "Harmonic" if name == "hh" else "Anharmonic"
                    plt.plot(result["E"] + delta, result["P"]/max(result["P"]),
                             "-", alpha=1, label=name, linewidth=3)
                    plt.vlines(result["PEAK2"], 0, 1, linestyles='dashed')
                    ticks.append(float("{:.2}".format(result["PEAK2"])))

plot_experiment("experiment.txt", "Sample #E (Cernet)")
plot_experiment("experiment2.txt", "Sample #T2 (Tokyo Denpa)")
plt.xlabel("$E$ (eV)")
plt.ylabel("Intensity")
plot_results()
ticks = sorted(ticks + list(np.arange(0, 3, 0.5)))
plt.xticks(ticks)
plt.grid()
plt.xlim([0.5, 2.9])
plt.ylim([0, 1.2])
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
           ncol=3, fancybox=True, shadow=True)
# plt.savefig("kakas.")
plt.savefig('spectrum_comparison.png', dpi=200)

plt.show()
