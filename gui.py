from __future__ import unicode_literals
import sys
import os
import random
from matplotlib.backends import qt4_compat
use_pyside = qt4_compat.QT_API == qt4_compat.QT_API_PYSIDE
if use_pyside:
    from PySide import QtGui, QtCore
else:
    from PyQt4 import QtGui, QtCore

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg\
    as FigureCanvas
from matplotlib.figure import Figure

progname = os.path.basename(sys.argv[0])
progversion = "0.1"

##############################
#      Local libraries
##############################

from sturm_liouvile_functions import functions
from numerov import find_roots
import numpy as np


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        self.compute_initial_figure()

        super().__init__(fig)

        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class Selector(QtGui.QComboBox):
    def __init__(self, fuctions, parent=None):
        super().__init__(parent)
        for i in fuctions:
            self.addItem(i, fuctions[i])


def calculate_classical_boundaries(sturm_liouvile, max_energy=10,
                                   delta_x=0, delta_e=0):
    x = []
    y = []

    for i in np.linspace(0, max_energy):
        roots = find_roots(lambda x: sturm_liouvile(x, energy=i))
        x.extend([k + delta_x for k in roots])
        y.extend([i - delta_e]*len(roots))
        y.append(- delta_e)
        x.append(delta_x)

    potencial = zip(x, y)
    potencial = sorted(potencial)
    potencial = np.array(potencial)
    x = potencial[:, 0]
    y = potencial[:, 1]

    return x, y


class PotencialsCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""

    def __init__(self, selector1, selector2, x, e, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.selector1 = selector1
        self.selector2 = selector2
        self.x = x
        self.e = e
        self.draw_potencials()

    def draw_potencials(self):

        data1 = self.selector1.itemData(self.selector1.currentIndex())
        data2 = self.selector2.itemData(self.selector2.currentIndex())
        sturm_liouvile1 = data1["fuction"]
        sturm_liouvile2 = data2["fuction"]

        x1, y1 = calculate_classical_boundaries(sturm_liouvile1)
        x2, y2 = calculate_classical_boundaries(sturm_liouvile2,
                                                delta_x=self.x.value(),
                                                delta_e=self.e.value())
        self.axes.plot(x1, y1, x2, y2)
        self.axes.grid(True)
        self.draw()

    def draw_eigenstates(self):
        data1 = self.selector1.itemData(self.selector1.currentIndex())
        data2 = self.selector2.itemData(self.selector2.currentIndex())
        data1_file = data1["file"]
        data2_file = data2["file"]

    def compute_initial_figure(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2*pi*t)
        self.axes.plot(t, s)


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""
    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):
        self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)
        l = [random.randint(0, 10) for i in range(4)]

        self.axes.plot([0, 1, 2, 3], l, 'r')
        self.draw()


class ApplicationWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Inner product calculator")

        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtGui.QWidget(self)

        main_layout = QtGui.QVBoxLayout(self.main_widget)
        self.first_selector = Selector(functions)
        self.second_selector = Selector(functions)

        self.form_layout = QtGui.QFormLayout()
        self.form_layout.addRow(self.tr("&First Potencial:"),
                                self.first_selector)
        self.form_layout.addRow(self.tr("&Second Potencial:"),
                                self.second_selector)
        self.delta_x = QtGui.QDoubleSpinBox()
        self.delta_x.setValue(1)
        self.delta_e = QtGui.QDoubleSpinBox()
        self.delta_e.setValue(1)
        self.form_layout.addRow(self.tr("&X difference:"),
                                self.delta_x)
        self.form_layout.addRow(self.tr("&E difference:"),
                                self.delta_e)

        main_layout.addLayout(self.form_layout)
        sc = PotencialsCanvas(self.first_selector, self.second_selector,
                              self.delta_x, self.delta_e,
                              self.main_widget, width=5, height=4, dpi=100)
        self.delta_x.valueChanged.connect(sc.draw_potencials)
        self.delta_e.valueChanged.connect(sc.draw_potencials)
        self.first_selector.currentIndexChanged.connect(sc.draw_potencials)
        self.second_selector.currentIndexChanged.connect(sc.draw_potencials)
        main_layout.addWidget(sc)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.statusBar().showMessage("All hail matplotlib!", 2000)

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtGui.QMessageBox.about(self, "About", "???")


qApp = QtGui.QApplication(sys.argv)

aw = ApplicationWindow()
aw.setWindowTitle("%s" % progname)
aw.show()
sys.exit(qApp.exec_())
#qApp.exec_()
