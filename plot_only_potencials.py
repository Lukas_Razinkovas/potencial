import pickle
import numpy as np
# from get_data import get_result, plot_potencial
# from scipy.constants import k
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

potencial = "hh"

# FWHM EXPERIMENTAL
T2 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180,
               200, 220, 230, 240, 260, 270, 280, 290, 300])

FWHM2 = np.array([0.507, 0.505, 0.509, 0.506, 0.515, 0.514, 0.522, 0.518, 0.52,
                  0.522, 0.543, 0.558, 0.571, 0.588, 0.606, 0.621, 0.629,
                  0.641, 0.661, 0.665, 0.677, 0.692, 0.704])

# PEAK EXPERIMENTAL

k = 8.6173324e-5


def coth(x):
    return (np.exp(x) + np.exp(-x))/(np.exp(x) - np.exp(-x))


def func2(T, homega, A):
    return A*np.sqrt(coth(homega/(2*k*T)))


shapes = ["s", "<", "D", "o", "."]


def plot_results(with_omega=True, plot="FWHM"):
    for name in ["hh"]:
        print(shapes)
        T = []
        FWHM = []
        PEAK = []
        with open("data/{}_results{}.p".format(
                name, "" if with_omega else "_no_w"), "rb") as data_file:
            results = pickle.loads(data_file.read())
            for result in results:
                input(result.keys())
                T.append(result["T"])
                FWHM.append(result["FWHM2"])
                PEAK.append(result["PEAK"])

            T[0] = 1
            T = np.array(T)
            if plot == "FWHM":
                FWHM = np.array(FWHM)
                popt, pcov = curve_fit(func2, T, FWHM, [0.0036, 6])
                plt.plot(T, func2(T, *popt))
                plt.plot(T, FWHM, shapes.pop(0),
                         label="$\hbar\omega_{{ \\mathrm{{ {} }} }}={:.3f},\\ "
                         "A_{{ \\mathrm{{ {} }} }}={:.3f}$".format(
                             name, popt[0], name, popt[1]))
            else:
                plt.plot(T, PEAK, shapes.pop(0),
                         label=name)

    if plot == "FWHM":
        popt, pcov = curve_fit(func2, T2, FWHM2, [0.0036, 6])
        plt.plot(T2, FWHM2, shapes.pop(),
                 label="$\hbar\omega_{{ \\mathrm{{ {} }} }}={:.3f},\\ "
                 "A_{{ \\mathrm{{ {} }} }}={:.3f}$".format(
                     "ex", popt[0], "ex", popt[1]))
        plt.plot(T, func2(T, *popt))
    else:
        T3 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180,
              200, 220, 230, 240, 260, 270, 280, 290, 300, 310, 320]

        PEAK3 = [1.955, 1.96, 1.96, 1.967, 1.973, 1.98, 1.983, 1.99, 1.996,
                 2.005, 2.01, 2.017, 2.024, 2.035, 2.037, 2.044, 2.052,
                 2.059, 2.067, 2.079, 2.085, 2.089, 2.096, 2.103, 2.11]

        plt.plot(T3, PEAK3, shapes.pop(),
                 label="EX")

plot_results(plot="FWHMs")

plt.legend(loc=2)
plt.grid(True)
plt.show()
