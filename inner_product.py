from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from numerov import normalize_result, integrate
import numpy as np


def calculate_dot_product(x1, y1, x2, y2):
    """Calculates inner product of two functions given as points.
    Itervals can differ, interpolation is used."""
    y1 = normalize_result(x1, y1)
    y2 = normalize_result(x2, y2)

    # plt.plot(x1, y1)
    # plt.plot(x2, y2)
    # print(integrate(x1, pow(y1, 2)))
    # plt.show()
    # input()

    # Choose x1 to be function with lowest x
    x1, x2, changed = ((x1[:], x2[:], False) if min(x1) < min(x2)
                       else (x2[:], x1[:], True))
    y1, y2 = (y2[:], y1[:]) if changed else (y1[:], y2[:])

    before_inter = [i for i in x1 if i < x2[0]]

    # Finding intersection interval
    inter = np.array([i for i in x1 if x2[0] <= i <= x2[-1]])
    f2 = interp1d(x2, y2)

    # Interpolating points of x2 to corespond to x1
    y1_inter = np.array(y1[len(before_inter): len(before_inter) + len(inter)])
    y2_inter = f2(inter)

    # Trapezoidal rule integration
    differences = inter[1:] - inter[:-1]
    integral = y1_inter * y2_inter
    trapecies = differences*(integral[1:] + integral[:-1])/2

    return trapecies.sum()
