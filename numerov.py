"""Module designed for solving differential equations integration of type:

. math::

   (\frac{d^2}{dx^2} + q(x))y = 0,

where q is called Sturm-Liouville function.
"""

# Some imports for python2 support
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

# Scientific libraries
import numpy as np

##############################
#      Finding roots
##############################


def find_roots(func, a=-100, b=100, delta=1e-6, nr_of_points=10000):
    """Finds where `func(x) = 0` in the interval [a,b].
    Works recursively.

    Arguments:
      func (function): function from one variable
      delta (float): precision
      nr_of_points (int): nr of division points
    """
    x = np.linspace(a, b, nr_of_points)
    y = func(x)

    # Isrenkame tik tuos intervalus tarp kuriu keiciasi zenklas
    changed = y[:-1] * y[1:] < 0
    intervals = zip(x[changed], x[1:][changed])

    roots = []
    for beg, end in intervals:
        if abs(end - beg) > delta:
            roots += find_roots(func, beg, end,
                                delta=delta,
                                nr_of_points=nr_of_points)
        else:
            roots.append(beg + (end - beg)/2)

    return roots


def determine_where_e_is_equal_to_v(sturm_liouvile_function,
                                    a=-100, b=100,
                                    delta_x=0.001):
    """Finds classical boundaries for given particle."""
    return find_roots(sturm_liouvile_function, a, b, delta_x)


##############################
#       Integration
##############################

def integrate(x, y):
    """Integrating y(x) with trapecies method."""
    x, y = np.array(x), np.array(y)
    differences = x[1:] - x[:-1]
    trapecies = differences*(y[1:] + y[:-1])/2
    return trapecies.sum()


def normalize_result(x, y):
    y = np.array(y)
    y = y/np.sqrt(integrate(x, pow(y, 2)))
    return y


##############################
#     Numerov Method
##############################

def psi_next(x, psi, psi_prev, h, sturm_liouvile_function):
    """Numerov formula for next step. Requires first two steps."""

    a_1 = 2*(1 - (5/12)*pow(h, 2)*sturm_liouvile_function(x))*psi
    a_2 = (1 + (1/12)*pow(h, 2)*sturm_liouvile_function(x-h))*psi_prev
    a_3 = 1 + (1/12)*pow(h, 2)*sturm_liouvile_function(x+h)

    return (a_1 - a_2)/a_3


def numerov_method(sturm_liouvile_function, N=6000,
                   reversed=False, rescale="normalize", scale=1,
                   borders=3):
    """Fixed step numerov integration.

    Arguments:
      sturm_liouvile_function (function): function with one scalar argument
      N (int, optional): number of steps. Defaults to 60000.
      reversed (bool, optional): if True integrates from -infty to infty.
        Defaults to False
      rescale (bool or str, optional): If True returns normalized result
              $\int|y(x)|^2 = 1$.
    Return:
      result (dict): dictionary containing energy and x, y points::

          {
              'x': <list of floats>,
              'y': <list of floats>,
              'roots': <list of floats>,
          }
    """

    #####################################
    #  Determine integration boundaries
    #####################################

    roots = determine_where_e_is_equal_to_v(sturm_liouvile_function)
    x_min_classical = roots[0]
    x_max_classical = roots[1]

    # [TODO]: protingesnio integravimo ribų nustatymo
    x_start = x_min_classical - borders
    x_end = x_max_classical + borders

    h = (x_end - x_start)/N

    # Reversed integration
    if reversed:
        h = -h
        x_start, x_end = x_end, x_start

    # Choose first two values
    x_out = [x_start-2*h, x_start-h]
    y_out = [0.000, 0.001]

    for i in range(N):
        y_out.append(psi_next(x_out[-1], y_out[-1], y_out[-2], h,
                              sturm_liouvile_function))
        x_out.append(x_out[-1] + h)

    if rescale == "normalize":
        y_out = normalize_result(x_out, y_out)
    else:
        y_out = np.array(y_out)
        y_out /= y_out.max()

    return {"x": x_out,
            "y": y_out,
            "roots": roots[:2]}
