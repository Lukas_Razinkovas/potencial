import pickle
import numpy as np
# from get_data import get_result, plot_potencial
# from scipy.constants import k
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

potencial = "hh"

# FWHM EXPERIMENTAL
T2 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180,
               200, 220, 230, 240, 260, 270, 280, 290, 300])

FWHM2 = np.array([0.507, 0.505, 0.509, 0.506, 0.515, 0.514, 0.522, 0.518, 0.52,
                  0.522, 0.543, 0.558, 0.571, 0.588, 0.606, 0.621, 0.629,
                  0.641, 0.661, 0.665, 0.677, 0.692, 0.704])

# PEAK EXPERIMENTAL

k = 8.6173324e-5


def coth(x):
    return (np.exp(x) + np.exp(-x))/(np.exp(x) - np.exp(-x))


def func2(T, homega, A):
    return A*np.sqrt(coth(homega/(2*k*T)))

def Line(T, m):
    a = 1.95
    return m*T + a

def Line2(T, m):
    a = 1.45
    return m*T + a


shapes = ["s", "<", "D", "o", "."]

# fig = plt.figure(figsize=(10, 7), dpi=100)
fig, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(10, 7), dpi=100)


def read_data(t="fwhm", nr=1):
    with open("results/" + t + str(nr) + ".txt", "rt") as f:
        x, y = [], []
        for line in f:
            try:
                e, p = line.rstrip().split("\t")
            except:
                print("results/" + t + str(nr) + ".txt")
                input(line)

            x.append(float(e))
            if t == "fwhm":
                y.append(float(p)/1000)
            else:
                y.append(float(p))

    return np.array(x), np.array(y)

def plot_results(with_omega=False, plot="FWHM"):
    for size, shape, name in [
            [9, "D", "hh"], [9, "D", "ha"], [9, "D", "aa"]]:
        T = []
        PEAK = []
        with open("data/{}_results{}.p".format(
                name, "" if with_omega else "_no_w"), "rb") as data_file:
            results = pickle.loads(data_file.read())
            for result in results:
                T.append(result["T"])
                PEAK.append(result["PEAK2"])

            T[0] = 1
            T = np.array(T)

            if name == "hh":
                # ax2.plot(T, PEAK, shapes.pop(0), label=name)
                popt, pcov = curve_fit(Line2, T, PEAK, [0])
                x = np.linspace(0, 350, 1000)
                ax2.plot(x, Line2(x, *popt), "--")
                         # label=name + " $m={:.2e}$".format(popt[0]))
                ax2.plot(T, PEAK, "rD", markersize=size, label="Harmonic")
            else:

                if name == "aa":
                    name = "Anharmonic"
                else:
                    name = "Harmonic-Anharmonic"
                popt, pcov = curve_fit(Line, T, PEAK, [0])
                x = np.linspace(0, 350, 1000)
                ax.plot(x, Line(x, *popt), "--")
                        # label=name + " $m={:.2e}$".format(popt[0]))
                ax.plot(T, PEAK, shape, markersize=size, label=name)

    FULL_T = []
    FULL_P = []
    for nr in [1, 2, 3, 4]:
        labels = {1: "Sample E", 2: "Sample B", 3: "Sample M27",
                  4: "Sample TD3212"}
        T2, FWHM2 = read_data("peak", nr=nr)
        ax.plot(T2, FWHM2, "o", label=labels[nr])
        if nr in {1, 2}:
            popt, pcov = curve_fit(Line, T2, FWHM2, [0])
            x = np.linspace(0, 350, 1000)
            ax.plot(x, Line(x, *popt), "--")
                    # label="fit{} $m={:.2e}$".format(nr, popt[0]))

        FULL_T += list(T2)
        FULL_P += list(FWHM2)

    # FULL_T = np.array(FULL_T)
    # FULL_P = np.array(FULL_P)
    # popt, pcov = curve_fit(line, FULL_T, FULL_P, [0.0036, 6])
    # print(popt, pcov)
    # x = np.linspace(0, 350, 1000)
    # ax.plot(x, line(x, *popt), "--")


plot_results(True, plot="FWHM")
# plt.subplots_adjust(hspace=0.03)

ax.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax.xaxis.tick_top()
ax.tick_params(labeltop='off') # don't put tick labels at the top
ax2.xaxis.tick_bottom()

d = .015 # how big to make the diagonal lines in axes coordinates
# arguments to pass plot, just so we don't keep repeating them
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
ax.plot((-d,+d),(-d,+d), **kwargs)      # top-left diagonal
ax.plot((1-d,1+d),(-d,+d), **kwargs)    # top-right diagonal

kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
ax2.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
ax2.plot((1-d,1+d),(1-d,1+d), **kwargs) # bottom-right diagonal

# ax.spines['bottom'].set_visible(False)
# ax2.spines['top'].set_visible(False)
# ax.xaxis.tick_top()
# ax.tick_params(labeltop='off')
# ax2.xaxis.tick_bottom()


ax2.set_xlabel("$T$ (K)")
ax2.set_ylabel("Peak (eV)")
ax2.yaxis.set_label_coords(0.05, 0.5, transform=fig.transFigure)

ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.3),
          ncol=3, fancybox=True, shadow=True, fontsize=12)
ax2.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
           ncol=3, fancybox=True, shadow=True, fontsize=12)


ax.set_ylim([1.93, 2.17])
ax2.set_ylim([1.4, 1.67])
ax.grid(True)
ax2.grid(True)
plt.savefig("peak.png", dpi=300)
plt.show()
