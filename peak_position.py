import pickle
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy.signal import find_peaks_cwt
import numpy as np


potencial = "ha"

with open("data/{}_results.p".format(potencial), "rb") as data_file:
    results = pickle.loads(data_file.read())


def gauss_function(x, a, mean, sigma):
    return a*np.exp(-(x-mean)**2/(2*sigma**2))


def get_params(e, p):
    mean0 = 1.5
    sigma0 = 0.2626928
    popt, pcov = curve_fit(gauss_function, e, p, p0=[1, mean0, sigma0])
    a, mean, sigma = popt
    PEAK = mean

    PEAK2 = e[find_peaks_cwt(p, np.arange(10, 15))]
    FWHM = 2.3548*sigma
    return PEAK, PEAK2, FWHM

FWHM = []
PEAK = []
PEAK2 = []
PEAK3 = []
T = []

for result in results:
    e, p, t = result["E"], result["P"]/max(result["P"]), result["T"]
    T.append(t)
    peak, peak2, f = get_params(e, p)

    FWHM.append(f)
    PEAK.append(peak)
    PEAK2.append(peak2)
    PEAK3.append(e[np.argmax(p)])
    plt.plot(e, p)
    plt.plot()



plt.grid(True)

plt.plot(T, PEAK)
plt.plot(T, PEAK, "yo")

plt.plot(T, PEAK2)
plt.plot(T, PEAK2, "bo")

plt.plot(T, PEAK3)
plt.plot(T, PEAK3, "ro")

plt.show()
