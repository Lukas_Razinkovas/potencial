from inner_product import calculate_dot_product
import numpy as np
import get_data as data
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit
from scipy.signal import find_peaks_cwt
from scipy.constants import physical_constants

hbar = physical_constants["Planck constant in eV s"][0]/(2 * np.pi)


def calculate_energy_spectrum(potencial1, potencial2, temp=270,
                              delta_x=15, delta_e=-60,
                              scale="angstrom_mass",
                              plt=None, points=11000,
                              plot_potencials=None,
                              params1=None, params2=None,
                              omega=True):

    if params1 is None:
        params1 = {}
    if params2 is None:
        params2 = {}

    results1 = data.get_result(potencial1, scale=scale, params=params1)
    results2 = data.get_result(potencial2, delta_x=delta_x, delta_e=delta_e,
                               scale=scale, params=params2)

    if plot_potencials:
        data.plot_potencials(results1, results2, eigenstates=True)

    Eterm, Pterm = data.calculate_distribution(
        results1, temp=temp)

    if plt:
        plt.plot(Pterm, label="$T={}$".format(temp))

    P = []
    PT = []
    E = []

    for nr, i in enumerate(results1["eigenspectrum"]):
        print("{}\r".format(nr), end="")

        for nr2, i2 in enumerate(results2["eigenspectrum"]):
            photon_energy = i["energy"] - i2["energy"]
            E.append(photon_energy)

            dot_product = calculate_dot_product(
                i["x"], i["y"], i2["x"], i2["y"])

            p = pow(dot_product, 2)
            # if nr < 2 and 2.1 < photon_energy < 2.2:
            #     if p > 0.01:
            #         print("second", nr, nr2, p)

            # if nr < 2 and 1.68 < photon_energy < 1.75:
            #     if p > 0.01:
            #         print("first", nr, nr2, p)

            P.append(p)
            # PT.append(p * Pterm[nr])
            if omega:
                print(omega)
                PT.append(p*Pterm[nr]*((photon_energy/hbar)**3))
            else:
                PT.append(p * Pterm[nr])

    # E -- photon energy
    # P -- overlap integral
    # PT -- statistically weighted overlap integral

    new_e = np.linspace(max(E), min(E), points)
    new_p = np.zeros(len(new_e))

    diff = (max(E) - min(E))/35
    diff = 0.025
    print(diff)

    for nr, (e, p) in enumerate(zip(E, PT)):
        distr = lambda x: p*norm.pdf(x, e, diff)
        gauss = distr(new_e)
        new_p += gauss

    return new_e, new_p


def params_to_title(params, nr):
    """Converts parameters dictionary to readable title."""

    my_list = []
    for i in params:
        if i == "omega":
            text = "$\\hbar\\omega_{}$ = {} eV".format(nr, params[i])
        elif i == "m":
            text = "$\mathrm{{{}}}_{}$ = {} amu".format(i, nr, params[i])
        else:
            text = "$\mathrm{{{}}}_{}$ = {}".format(i, nr, params[i])

        my_list.append(text)

    my_list.sort()
    my_list.reverse()
    return ", ".join(my_list)


def find_fwhm(xn, yn):
    yn = yn - yn.max()/2

    roots = []
    sign = -1
    for x, y in zip(xn, yn):
        if y/abs(y) != sign:
            roots.append(x)
            sign = y/abs(y)

    assert len(roots) == 2, "More than two roots found!!! {}".format(roots)
    return abs(roots[0] - roots[1])


def find_max(E, P):
    max_indx = np.argmax(P)
    return E[max_indx], P[max_indx]

    my_spline = UnivariateSpline(E[max_indx - 10: max_indx + 10],
                                 P[max_indx - 10: max_indx + 10])

    x = np.linspace(E[max_indx - 10],
                    E[max_indx + 10], 100)
    y = my_spline(x)
    indx = np.argmax(y)

    return x[indx], y[indx]


def gauss_function(x, a, mean, sigma):
    return a*np.exp(-(x-mean)**2/(2*sigma**2))


def get_params(E, P):
    skip = len(E)/8
    e = E[skip: -skip]
    p = P[skip: -skip]
    mean0 = 1.5
    sigma0 = 0.2626928
    popt, pcov = curve_fit(gauss_function, e, p, p0=[max(p), mean0, sigma0])
    a, mean, sigma = popt
    PEAK = mean
    FWHM = 2.3548*sigma
    gauss_x = np.linspace(E[0], E[-1], 150)
    gauss_y = gauss_function(gauss_x, *popt)
    return PEAK, gauss_function(PEAK, *popt), FWHM, [gauss_x, gauss_y]


def calculate_temperature_dependance(potencial1, potencial2, delta_x, delta_e,
                                     start=0, stop=800, step=100,
                                     plot_potencials=False, omega=True):
    results = []

    maxim = None
    for nr, temp in enumerate(range(start, stop+1, step)):
        print("* calculating for temperature {}K...".format(temp))

        E, P = calculate_energy_spectrum(
            potencial1["name"], potencial2["name"],
            delta_x=delta_x, delta_e=delta_e,
            plot_potencials=True if nr == 0 else False,
            temp=temp, params1=potencial1, params2=potencial2, plt=plt,
            omega=omega)

        if maxim is None:
            maxim = max(P)

        P = P/maxim

        peak, peak_y, fwhm, gauss = get_params(E, P)
        # PEAK3 = E[find_peaks_cwt(P, np.arange(10, 15))]
        # PEAK3_Y = P[find_peaks_cwt(P, np.arange(10, 15))]

        results.append({
            "T": temp,
            "E": E,
            "P": P,
            "FWHM": fwhm,
            "FWHM2": find_fwhm(E, P),
            "PEAK": peak,
            "PEAK_Y": peak_y,
            "PEAK2": find_max(E, P)[0],
            "PEAK_Y2": find_max(E, P)[1],
            # "PEAK3": PEAK3,
            # "PEAK_Y3": PEAK3_Y,
            "params1": potencial1,
            "params2": potencial2,
            "gauss": gauss,
        })

    plt.xlim([0, 10])
    plt.legend()
    plt.show()
    return results


def plot_temperature_dependance(results, plot=True, save=None, figsize=None):
    if figsize is None:
        figsize = (8, 22)

    # fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, figsize=figsize)

    print("Gauss")

    for result in results:
        plt.plot(result["E"], result["P"])
        plt.plot(result["PEAK"], result["PEAK_Y"], "y.", label="GAUSS")
        # plt.plot(gauss[0], gauss[1], ".")

    plt.grid(True)
    plt.ylabel("Intensity")
    plt.xlabel("$\hbar\omega$ (eV)")
    fwhm = result["FWHM"]
    peak = result["PEAK"]
    plt.xlim([peak - 2*fwhm, peak + 2*fwhm])
    plt.show()

    print("MAX")

    for result in results:
        plt.plot(result["E"], result["P"])
        plt.plot(result["PEAK2"], result["PEAK_Y2"], "b.", label="MAX")

    plt.grid(True)
    plt.ylabel("Intensity")
    plt.xlabel("$\hbar\omega$ (eV)")
    fwhm = result["FWHM"]
    peak = result["PEAK"]
    plt.xlim([peak - 2*fwhm, peak + 2*fwhm])
    plt.show()

    # print("SIGNAL")

    # for result in results:
    #     plt.plot(result["E"], result["P"])
    #     plt.plot(result["PEAK3"], result["PEAK_Y3"], "g.", label="SIGNAL")

    # plt.grid(True)
    # plt.ylabel("Intensity")
    # plt.xlabel("$\hbar\omega$ (eV)")
    # fwhm = result["FWHM"]
    # peak = result["PEAK"]
    # plt.xlim([peak - 2*fwhm, peak + 2*fwhm])
    # plt.show()

    print("Gauss")
    plt.plot([i["T"] for i in results],
             [i["FWHM"] for i in results])
    plt.plot([i["T"] for i in results],
             [i["FWHM"] for i in results], "b.")

    plt.xlabel("T (K)")
    plt.ylabel(r"FWHM (meV)")
    plt.grid(True)
    plt.show()

    print("True")
    plt.plot([i["T"] for i in results],
             [i["FWHM2"] for i in results])
    plt.plot([i["T"] for i in results],
             [i["FWHM2"] for i in results], "b.")

    plt.xlabel("T (K)")
    plt.ylabel(r"FWHM (meV)")
    plt.grid(True)
    plt.show()

    print("Gauss")

    plt.plot([i["T"] for i in results],
             [i["PEAK"] for i in results])
    plt.plot([i["T"] for i in results],
             [i["PEAK"] for i in results], "b.")

    plt.xlabel("T (K)")
    plt.ylabel(r"Peak Position (eV)")
    plt.grid(True)
    plt.show()

    print("MAX")

    plt.plot([i["T"] for i in results],
             [i["PEAK2"] for i in results])
    plt.plot([i["T"] for i in results],
             [i["PEAK2"] for i in results], "b.")
    plt.xlabel("T (K)")
    plt.ylabel(r"Peak Position (eV)")
    plt.grid(True)
    plt.show()

##############################
#        Testukai
##############################

if __name__ == "__main__":
    potencial1 = {
        "name": "harmonic",
        "omega": 0.03,
        "m": 4
    }

    potencial2 = params1 = {
        "name": "harmonic",
        "omega": 0.03,
        "m": 4
    }

    DELTA_Q = 1.7  # angstrom
    DELTA_E = -2   # eV

    result = calculate_temperature_dependance(potencial1, potencial2,
                                              DELTA_Q, DELTA_E,
                                              start=0, step=200, stop=400)

    plot_temperature_dependance(result)
