import pickle
import numpy as np
# from get_data import get_result, plot_potencial
# from scipy.constants import k
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

potencial = "hh"

# FWHM EXPERIMENTAL
T2 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180,
               200, 220, 230, 240, 260, 270, 280, 290, 300])

FWHM2 = np.array([0.507, 0.505, 0.509, 0.506, 0.515, 0.514, 0.522, 0.518, 0.52,
                  0.522, 0.543, 0.558, 0.571, 0.588, 0.606, 0.621, 0.629,
                  0.641, 0.661, 0.665, 0.677, 0.692, 0.704])

# PEAK EXPERIMENTAL

k = 8.6173324e-5


def coth(x):
    return (np.exp(x) + np.exp(-x))/(np.exp(x) - np.exp(-x))


def func2(T, homega, A):
    return A*np.sqrt(coth(homega/(2*k*T)))

def line(T, m, a):
    return m*T + a


shapes = ["s", "<", "D", "o", "."]

fig = plt.figure(figsize=(10, 7), dpi=100)
ax = plt.subplot(111)


def read_data(t="fwhm", nr=1):
    with open("results/" + t + str(nr) + ".txt", "rt") as f:
        x, y = [], []
        for line in f:
            try:
                e, p = line.rstrip().split("\t")
            except:
                print("results/" + t + str(nr) + ".txt")
                input(line)

            x.append(float(e))
            if t == "fwhm":
                y.append(float(p)/1000)
            else:
                y.append(float(p))

    return np.array(x), np.array(y)


def plot_results(with_omega=False, plot="FWHM"):
    for size, shape, name in [
            [11, "D", "hh"], [11, "s", "ha"], [11, "<", "aa"]]:
        print(shapes)
        T = []
        FWHM = []
        PEAK = []
        with open("data/{}_results{}.p".format(
                name, "" if with_omega else "_no_w"), "rb") as data_file:
            results = pickle.loads(data_file.read())
            for result in results:
                T.append(result["T"])
                FWHM.append(result["FWHM2"])
                PEAK.append(result["PEAK"])

            T[0] = 1
            T = np.array(T)
            if plot == "FWHM":
                # FWHM = np.array(FWHM)
                # if name == "aa":
                #     FWHM += 0.13
                # elif name == "hh":
                #     FWHM -= 0.11
                # else:
                #     continue

                popt, pcov = curve_fit(func2, T, FWHM, [0.0036, 6])
                ax.plot(T, func2(T, *popt))
                ax.plot(T, FWHM, shape, alpha=1, markersize=size,
                        label="$\hbar\omega_{{ \\mathrm{{ {} }} }}={:.4f},\\ "
                        "A_{{ \\mathrm{{ {} }} }}={:.4f}$".format(
                            name, popt[0], name, popt[1]))
            else:
                ax.plot(T, PEAK, shapes.pop(0), label=name)

    if plot == "FWHM":
        if with_omega:
            for nr in [1, 2, 3, 4]:
                labels = {1: "Sample E", 2: "Sample B", 3: "Sample M27",
                          4: "Sample TD3212"}
                T2, FWHM2 = read_data(nr=nr)
                # popt, pcov = curve_fit(func2, T2, FWHM2, [0.0036, 6])
                ax.plot(T2, FWHM2, "o", label=labels[nr])

            for nr in [5, 6]:
                T2, FWHM2 = read_data(nr=nr)
                popt, pcov = curve_fit(func2, T2, FWHM2, [0.0036, 6])
                x = np.linspace(0, 350, 1000)
                lab = ("$\hbar\omega_{{ \\mathrm{{ {} }} }}={:.4f},\\ "
                       "A_{{ \\mathrm{{ {} }} }}={:.4f}$".format(
                           "fit{}".format(nr-4), popt[0],
                           "fit{}".format(nr-4), popt[1]))
                ax.plot(x, func2(x, *popt), "--", label=lab)
    else:
        for nr in [1, 2, 3, 4]:
            labels = {1: "Sample E", 2: "Sample B", 3: "Sample M27",
                      4: "Sample TD3212"}
            T2, FWHM2 = read_data("peak", nr=nr)
            # popt, pcov = curve_fit(func2, T2, FWHM2, [0.0036, 6])

            ax.plot(T2, FWHM2, "o", label=labels[nr])

        # T3 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180,
        #       200, 220, 230, 240, 260, 270, 280, 290, 300, 310, 320]

        # PEAK3 = [1.955, 1.96, 1.96, 1.967, 1.973, 1.98, 1.983, 1.99, 1.996,
        #          2.005, 2.01, 2.017, 2.024, 2.035, 2.037, 2.044, 2.052,
        #          2.059, 2.067, 2.079, 2.085, 2.089, 2.096, 2.103, 2.11]

        # ax.plot(T3, PEAK3, "o",
        #          label="EX")
        # ax.xlim([0, 350])
        # ax.ylim([1.9, 2.15])

plot_results(True, plot="FWHM")
ax.set_xlabel("$T$ (K)")
ax.set_ylabel("FWHM (eV)")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
          ncol=3, fancybox=True, shadow=True, fontsize=12)
ax.grid(True)
plt.savefig("fwhm.png", dpi=300)
plt.show()
