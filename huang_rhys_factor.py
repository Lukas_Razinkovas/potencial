from matplotlib import pyplot as plt
import numpy as np
from get_data import plot_potencial, get_result, plot_eigenspectrum
from scipy.optimize import curve_fit

DELTA_Q = 1.3
DELTA_E = 0

import matplotlib
from matplotlib import rc

matplotlib.rcParams['savefig.dpi'] = 2.5 * matplotlib.rcParams['savefig.dpi']

# rc('font', **{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
# matplotlib.rcParams['text.usetex'] = True


def get_from_dat(name):
    with open("data/{}.dat".format(name), "rt") as f:
        XA = []
        EA = []
        for i in f:
            try:
                X, E = i.rstrip().split(" ")
            except:
                pass
            XA.append(float(X))
            EA.append(float(E) - 0.46016292 + DELTA_E)
    return np.array(XA), np.array(EA)


XA, EA = get_from_dat("ground_a")
XAe, EAe = get_from_dat("excited_a")
# plt.plot(XA, EA, ".")
# plt.plot(XA, EA, 'ro', markersize=7, alpha=0.5)
# plt.plot(XAe, EAe, 'bo', markersize=7, alpha=0.5)


# def anharmonic_potencial_real(x, omega, beta, gamma):
#     F = 239.225344
#     return (0.5*F*pow(omega, 2)*pow(x, 2) + beta*pow(x, 3)
#             + abs(gamma)*pow(x, 4))

# params = [0.0308, -0.004, 0]
# new_e = anharmonic_potencial_real(XAe, *params)

# popt, pcov = curve_fit(anharmonic_potencial_real, XAe, EAe, params)
# print(popt)

# X = np.linspace(-5, 5, 100)
# plt.plot(X, anharmonic_potencial_am(X, *[240.966, 0.0308, -0.004, 0]), "--")
# plt.plot(X, anharmonic_potencial_real(X, *[0.026, -0.0034, 0]), "y--")
# plt.plot(X, anharmonic_potencial_real(X, *popt), "-")


# anharmonic = get_result("anharmonic", scale="angstrom_mass",
#                         delta_x=DELTA_Q, delta_e=DELTA_E)
# anharmonic2 = get_result("anharmonic2", scale="angstrom_mass",
#                          delta_x=0, delta_e=0)


potencial1 = {
    "name": "harmonic",
    "omega": 0.1,
    "m": 40,
}

potencial2 = {
    "name": "harmonic",
    "omega": 0.1,
    "m": 40
}

# plot_potencial(anharmonic, marker="r--", show=False, alpha=1, linewidth=2)
# plot_potencial(anharmonic2, marker="b--", show=False, alpha=1, linewidth=2)

harmonic = get_result("harmonic", scale="angstrom_mass",
                      delta_x=0, delta_e=0, params=potencial1)
harmonic2 = get_result("harmonic", scale="angstrom_mass",
                       delta_x=DELTA_Q, delta_e=DELTA_E, params=potencial2)

plot_potencial(harmonic, marker="b-", show=False, alpha=1, linewidth=4)
plot_potencial(harmonic2, marker="r-", show=False, alpha=1, linewidth=4)
plot_eigenspectrum(harmonic, show_energy=False, each=5)
plot_eigenspectrum(harmonic2, show_energy=False, each=100)

# plt.text(0.5, 1, r"$\displaystyle\mathrm{Li}_{\mathrm{Zn}}^0 + e^-$",
#          fontsize=22)
# plt.text(-1, -2.5, r"$\displaystyle\mathrm{Li}_{\mathrm{Zn}}^-$",
#          fontsize=24)
# plt.text(-0.5, 1, r"$\mathrm{Li}_{\text{Zn}}^0 + e^-$")


plt.xlim([-1.7, 2.99])
plt.ylim([-0.7, 3.2])
# plot_potencial(result2, marker="r-", label="anharmoninis $F=240$", show=False)

# plt.hlines(0, -10, 0, linestyles='dashed')
# plt.hlines(DELTA_E, -10, DELTA_Q, linestyles='dashed')

plt.annotate(
    "", (-3.5, DELTA_E), (-3.5, 0),
    arrowprops={'arrowstyle': '<->'})
plt.text(-3.4, -1.7, "$\displaystyle E_{\mathrm{ZPL}}$",
         fontsize=25)

plt.vlines(0, -10, 0, linestyles='dashed')
plt.vlines(DELTA_Q, -10, DELTA_E, linestyles='dashed')

plt.annotate(
    "", (0, -0.15), (DELTA_Q, -0.15),
    arrowprops={'arrowstyle': '<->'}, fontsize=20)

plt.text(
    0.5, -0.43,
    r" $\displaystyle\Delta Q$", fontsize=25)

# plt.text(
#     -0.32, 0.5,
#     r" $\displaystyle\omega$", fontsize=40)

# plt.text(
#     DELTA_Q - 0.32, 0.5,
#     r" $\displaystyle\omega$", fontsize=40)



# # Atvaizduojami potencialai
# plot_potencial(result1, result2, eigenstates=False, axes=plt, show=False)
# plt.savefig("liccd.jpeg")
plt.xlabel("$Q$", fontsize=20)
plt.ylabel("$E$", fontsize=20)
plt.grid(False)
plt.savefig("ccm.jpeg")
plt.show()
# plt.xlim([-4, 4])
# plt.ylim([-1, 2])
# plt.legend()

