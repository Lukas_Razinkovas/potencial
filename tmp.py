import pickle
import numpy as np
# from get_data import get_result, plot_potencial
from scipy.constants import k
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
from numpy import cosh, sinh
from scipy.optimize import minimize

potencial = "hh"

import csv


T = []
FWHM = []
PEAK = []

T_AA = []
FWHM_AA = []
PEAK_AA = []

# with open('/home/lukas/programming/potencial/data/csv/hh/fwhm.csv',
#           'rt') as csvfile:
#     spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in spamreader:
#         FWHM.append(float(row[0]))
#         T.append(float(row[1]))


with open("data/hh_results.p", "rb") as data_file:
    results = pickle.loads(data_file.read())

for result in results:
    T.append(result["T"])
    FWHM.append(result["FWHM2"])
    PEAK.append(result["PEAK2"])


with open("data/aa_results.p", "rb") as data_file:
    results = pickle.loads(data_file.read())

for result in results:
    T_AA.append(result["T"])
    FWHM_AA.append(result["FWHM2"])
    PEAK_AA.append(result["PEAK2"])

T_T = [
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    120,
    140,
    160,
    180,
    200,
    220,
    230,
    240,
    260,
    270,
    280,
    290,
    300,
    310,
    320
]

PEAK_T = [
    1.955,
    1.96,
    1.96,
    1.967,
    1.973,
    1.98,
    1.983,
    1.99,
    1.996,
    2.005,
    2.01,
    2.017,
    2.024,
    2.035,
    2.037,
    2.044,
    2.052,
    2.059,
    2.067,
    2.079,
    2.085,
    2.089,
    2.096,
    2.103,
    2.11
]

plt.plot(T, PEAK, "r-", label="teorinis harmoninis")
plt.plot(T_AA, PEAK_AA, "b-", label="teorinis anharmoninis")
plt.plot(T_T, PEAK_T, ".", label="eksperimentas")
plt.xlim([0, 350])
plt.legend()
plt.show()
